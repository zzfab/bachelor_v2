config = {
    'model_default': {
        'rf': {
             'n_estimators' : 1000,
             'bootstrap' : True,
             'max_features' :'sqrt'
        },
        'logistic' : {
            'n_estimators': 1000
        },
        'cnn':{
            'num_filters' : 8,
            'filter_size' : 3,
            'pool_size' : 2,
            'activation': 'softmax'
        }

    },
	'dim_reduce': {
		'n_components' : 2,
		'seed' : 42,
        'perplexity' : [2,10]
	}
}