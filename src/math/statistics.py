from scipy import stats
from statsmodels.stats.multitest import multipletests
import numpy as np


def ttest(dictionary):
    key_dic = {}
    for key in dictionary.keys():
        pval_dic = {}
        rvs1 = dictionary[key][dictionary[key]['target'] == 1]
        rvs0 = dictionary[key][dictionary[key]['target'] == 0]
        for i in dictionary[key].drop('target',axis=1).columns:
             stat , pvalue = stats.ttest_ind(rvs1[i], rvs0[i])
             pval_dic[i] =  pvalue
        key_dic[key] = pval_dic

    print("[+].src.math.statistics.bonferroni_corretion: ttest done ")
    return key_dic

def bonferroni_correction(dicitionary,alpha=0.05):
    pval_dic = ttest(dicitionary)
    bfc_dic = {}

    for key in pval_dic.keys():
        reject, pvals_corr, _ ,alphacBonf = multipletests(np.array(list(pval_dic[key].values())),alpha,method = 'bonferroni')
        bfc_dic[key] = pvals_corr
        count = 0
        for item in pvals_corr:
            if item < alphacBonf:
                count += 1
        print("[+].src.math.statistics.bonferroni_corretion: significant pvals {} for {}".format(count,key))
    return bfc_dic

