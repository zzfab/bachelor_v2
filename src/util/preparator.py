import sys
import os
import logging
import pandas as pd
from settings import setting
from src.util.helper import read_data,convert_to_pkl,reduce_mem_usage
import re
from sklearn.model_selection import train_test_split
import time
import numpy as np


def union(dictionary):
    t0 = time.time()
    union_df = {}
    for pos_key,pos_val in dictionary.items():

        for neg_key,neg_val in dictionary.items():
            if 'pos' in pos_key and 'dna' in pos_key:
                # create target
                df_pos = dictionary[pos_key]
                df_pos['target'] = 1
                pos_int, pos_sf_int = (re.findall("(\d+)", pos_key))[0], (re.findall("(\d+)", pos_key))[1]
                if 'neg' in neg_key and 'dna' in neg_key:
                   # create target
                   df_neg = dictionary[neg_key]
                   df_neg['target']  = 0
                   neg_int , neg_sf_int = (re.findall("(\d+)", neg_key))[0],(re.findall("(\d+)", neg_key))[1]
                   if pos_int == neg_int and neg_sf_int == pos_sf_int:
                       #union pos and neg dataframe
                       union_df['dna_'+ str(pos_int) + "_" + str(pos_sf_int)] = pd.concat([df_pos.set_index('Unnamed: 0'),
                                                                   df_neg.set_index('Unnamed: 0')])
            elif 'pos' in pos_key and 'protein' in pos_key:
                # create target
                df_pos = dictionary[pos_key]
                df_pos['target'] = 1
                pos_int, pos_sf_int = (re.findall("(\d+)", pos_key))[0], (re.findall("(\d+)", pos_key))[1]
                if 'neg' in neg_key and 'protein' in neg_key:
                   # create target
                   df_neg = dictionary[neg_key]
                   df_neg['target']  = 0
                   neg_int, neg_sf_int = (re.findall("(\d+)", neg_key))[0], (re.findall("(\d+)", neg_key))[1]
                   #merge dataframes with same resolution
                   if pos_int == neg_int and neg_sf_int==pos_sf_int:
                       #union pos and neg dataframe
                       union_df['protein_'+ str(pos_int) +"_" + str(pos_sf_int)] = pd.concat([df_pos.set_index('Unnamed: 0'),
                                                                       df_neg.set_index('Unnamed: 0')])
    print("[+].src.util.preparator: union dataframes done")
    print(f"[+].src.util.preperator: took {time.time() - t0}")
    return union_df


def prepare_training(dictionary):
    print("[+].src.util.prepare_training: start ")
    t0 = time.time()
    prep_dic = {}
    for frame_name,dataframe in dictionary.items():
         #dataframe = reduce_mem_usage(dataframe)

         target = dataframe['target']
         data = dataframe.drop(['target'],axis=1)
         X_train, X_test,y_train,y_test = train_test_split(data,target,test_size=0.2, random_state=42)
         X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.25, random_state=42)  # 0.25 x 0.8 = 0.2

         split_set = {'X_train': X_train,
                     'X_test' : X_test,
                     'y_train' : y_train,
                     'y_test' : y_test,
                     'X_val' : X_val,
                     'y_val' : y_val }
         prep_dic[frame_name] = split_set

    print(f"[+].src.util.preparator: prepare training done in {time.time() - t0}")
    return prep_dic


