# #from weasyprint import HTML
# from settings import setting
# import os
# from datetime import datetime as dt
# from src.util.html_parser import parse_img_to_html
# import base64
# import os
# import img2pdf
# from argparse import ArgumentParser
# from reportlab.pdfgen.canvas import Canvas
# from reportlab.lib.pagesizes import landscape, A4
# try:
#     from PIL import Image
# except ImportError:
#     print('ehm')
#
#
# IMAGE_EXTENSIONS = ('jpg', 'jpeg', 'gif', 'png')
# A4_WIDTH, A4_HEIGHT = A4
#
#
# def is_wide_image(imagepath):
#     if not Image: return False
#
#     im = Image.open(imagepath)
#     res = im.size[0] > im.size[1]
#     del im  # implicit memory release
#     return res
#
#
# def split_wide_image(imagepath):
#     if not Image: return False
#     if not is_wide_image(imagepath): return False
#
#     name, ext = os.path.splitext(imagepath)
#
#     im = Image.open(imagepath)
#     width, height = im.size
#     im.crop((0, 0, int(width/2), height)).save('%s_0%s' % (name, ext))
#     im.crop((int(width/2), 0, width, height)).save('%s_1%s' % (name, ext))
#
#     del im
#     os.unlink(imagepath)
#     return True
#
#
# def get_image_size(imagepath):
#     if not Image: return False
#
#     im = Image.open(imagepath)
#     res = im.size
#     del im
#
#     return res
#
# class PdfBuilder(object):
#     def __init__(self, filename, pagesize=A4, fill=False):
#         self.pagesize = pagesize
#         self.filename = filename
#         self.canvas = Canvas(filename, pagesize)
#         self.fill = fill
#
#     def add_image(self, imagepath, horizon=False):
#         if self.fill:
#             width, height = self.pagesize
#             if horizon:
#                 width, height = height, width
#         else:
#             width, height = get_image_size(imagepath)
#
#         self.canvas.setPageSize((width, height))
#         self.canvas.drawImage(imagepath, 0, 0, width=width, height=height,
#                               preserveAspectRatio=True)
#         self.canvas.showPage()
#
#     def save(self):
#         self.canvas.save()
#
#
# def get_image_list(path):
#     for root, dirs, files in os.walk(path):
#         if dirs: continue
#
#         for filename in files:
#             ext = os.path.splitext(filename)[1][1:]
#             if ext.lower() in IMAGE_EXTENSIONS:
#                 yield os.path.join(root, filename)
#
#
# def create_pdf():
#     global Image
#
#
#     if True: Image = None  # turn off rotation
#
#     pagesize = A4
#     if True: pagesize = landscape(A4)
#
#     if True:
#         splitn = filter(lambda x: x is True, \
#                         map(split_wide_image, get_image_list(setting['paths']['reports'])))
#        # print('splitted %d images' % len(splitn))
#
#     pdf = PdfBuilder(os.path.join(setting['paths']['reports'],'output.pdf'), pagesize=pagesize, fill=True)
#     for imagename in sorted(get_image_list(setting['paths']['reports'])):
#         print   (' Adding image %s' % repr(imagename))
#         pdf.add_image(imagename, horizon=is_wide_image(imagename))
#
#
#     pdf.save()
#     print('[+].src.util.pdf_generator: save pdf')
#
# if __name__ == '__main__':
#     create_pdf()

