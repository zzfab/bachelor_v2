from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier

from sklearn import svm
from joblib import dump, load
import os
from src.config.config import config
from settings import setting
from sklearn.linear_model import RidgeClassifierCV,RidgeClassifier
from keras.models import Sequential
from keras.layers import Input, Conv1D, Dense, concatenate, Flatten
import time

import tensorflow as tf
config_2 = tf.compat.v1.ConfigProto(intra_op_parallelism_threads=0,
                                    inter_op_parallelism_threads=2,
                                    allow_soft_placement=True)

session = tf.compat.v1.Session(config=config_2)

def train_ridge_cl(dictionary,config):
    print("[+].train_ridge_cl: start")
    t0 = time.time()
    model = RidgeClassifier().fit(dictionary['X_train'], dictionary['y_train'].values.ravel())
    print(f"[+].train_ridge_cl: done in {time.time() - t0}")
    return model


def train_ridge_cl_cv(dictionary,config):
    print("[+].train_ridge_cl_cv: start")
    t0 = time.time()
    model = RidgeClassifierCV().fit(dictionary['X_train'], dictionary['y_train'].values.ravel())
    print(f"[+].train_ridge_cl_cv: done in {time.time() - t0}")
    return model

def train_cnn(dictionary, config):
    print("[+].train_cnn: start")
    t0 = time.time()
    X_train = dictionary['X_train']

    X_test = dictionary['X_test']
    from keras.utils import to_categorical
    # one-hot encode target column
    y_train = to_categorical(dictionary['y_train'])
    y_test = to_categorical(dictionary['y_test'])

    verbose, epochs, batch_size = 0, 50, 32
    model = Sequential()
    # add model layers
    model.add(Dense(10, activation='tanh', input_dim=dictionary['X_train'].shape[1]))
    model.add(Dense(9, activation='tanh'))
    model.add(Dense(8, activation='tanh'))
    model.add(Dense(2, activation='tanh'))
    # compile model using accuracy to measure model performance
    model.compile(optimizer='adam', loss='mean_squared_error', metrics=['accuracy'])
    # fit network
    history = model.fit(X_train,
                        y_train,
                        validation_data=(X_test, y_test),
                        epochs=epochs,
                        # batch_size=batch_size,
                        verbose=verbose)

    print(f"[+].train_cnn: done in {time.time() - t0}")
    return model


def train_svm_scr(dictionary,config):
    print("[+].train_svm_scr: start")
    t0 = time.time()
    model = svm.SVC()
    # Fit on training data
    model.fit(dictionary['X_train'],dictionary['y_train'].values.ravel())
    print(f"[+].train_svm_scr: done in {time.time() - t0}")
    return model
def train_logistic_regressin(dictionary,config):
    print("[+].train_logistic_regressin: start")
    t0 = time.time()
    #fit logistic regression
    logreg = LogisticRegression(#**config['logistic_default']
                                )
    model = logreg.fit(dictionary['X_train'],dictionary['y_train'].values.ravel())
    print(f"[+].train_logistic_regressin: done in {time.time() - t0}")
    return model

def train_random_forest(dictionary,config):
    print("[+].train_random_forest: start")
    t0 = time.time()
    # Create the model with 100 trees
    model = RandomForestClassifier(**config['model_default']['rf'])
    # Fit on training data
    model.fit(dictionary['X_train'],dictionary['y_train'].values.ravel())

    print(f"[+].train_random_forest: done in {time.time() - t0}")
    return model



def run_training(dic):
    #loop over protein/dna resolution df
    for key in dic.keys():
        #trian models
        print('*'*20 + "Start training for {}".format(key) + '*'*20)
        if setting['models']['rf']:
            rf_model = train_random_forest(dic[key],config)
        if setting['models']['lr']:
            lr_model = train_logistic_regressin(dic[key],config)
        if setting['models']['svm']:
            svm_scr = train_svm_scr(dic[key],config)
        if setting['models']['cnn']:
            cnn_model = train_cnn(dic[key], config)
        if setting['models']['ridge_cv']:
            ridge_cl_cv = train_ridge_cl_cv(dic[key], config)
        if setting['models']['ridge']:
            ridge_cl = train_ridge_cl(dic[key], config)

        #DUMP models
        if setting['models']['lr']:
            dump(lr_model,os.path.join(setting['paths']['models'],'lr_mod_{}.joblib'.format(key)))
        if setting['models']['rf']:
            dump(rf_model,os.path.join(setting['paths']['models'],'rf_mod_{}.joblib'.format(key)))
        if setting['models']['svm']:
            dump(svm_scr, os.path.join(setting['paths']['models'], 'svm_mod_{}.joblib'.format(key)))
        if setting['models']['cnn']:
            dump(cnn_model, os.path.join(setting['paths']['models'], 'cnn_mod_{}.joblib'.format(key)))
        if setting['models']['ridge_cv']:
            dump(ridge_cl_cv,os.path.join(setting['paths']['models'], 'ridge_cv_mod_{}.joblib'.format(key)))
        if setting['models']['ridge']:
            dump(ridge_cl,os.path.join(setting['paths']['models'], 'ridge_mod_{}.joblib'.format(key)))


