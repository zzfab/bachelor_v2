import logging
import os
import sys
import pandas as pd
import pickle
from settings import setting
import numpy as np

def convert_to_pkl(setting):
    """
    converts every file in folder to a pkl file and deletes csv
    :return:
    """
    for i in os.listdir(setting['paths']['data']):
        if i.endswith('.csv'):
            df = pd.read_csv(os.path.join(setting['paths']['data'],i))
            df.to_pickle(os.path.join(setting['paths']['data'],i).replace('.csv','.pkl'))
            os.remove(os.path.join(setting['paths']['data'],i))
        else:
            pass


def read_data(data_path):
    data_dic = {}
    sum_mem = 0
    for i in os.listdir(data_path):
        if i.endswith('.pkl'):
            data_dic[i.replace('.pkl','')] = pd.read_pickle(os.path.join(data_path,i))
            sum_mem += data_dic[i.replace('.pkl','')].memory_usage().sum() / 1024 ** 2

    print(('Memory usage of dataframes is {:.2f}'
           'MB').format(sum_mem))
    return data_dic


def iter_red_mem(dictionary):
    prep_dic = {}
    for frame_name,dataframe in dictionary.items():
        prep_dic[frame_name] = reduce_mem_usage(dataframe,frame_name)
        prep_dic[frame_name].to_pickle(os.path.join(setting['paths']['prep_data'],f'{frame_name}.pkl'))
    return prep_dic




def reduce_mem_usage(df,frame_name):
    """
    iterate through all the columns of a dataframe and
    modify the data type to reduce memory usage.
    """
    start_mem = df.memory_usage().sum() / 1024 ** 2
    print(('Memory usage of dataframe {} is {:.2f}'
           'MB').format(frame_name,start_mem))
    dataframe = df.astype(np.uint16)

    end_mem = dataframe.memory_usage().sum() / 1024 ** 2
    print(('Memory usage after optimization is: {:.2f}'
           'MB').format(end_mem))
    print('Decreased by {:.1f}%'.format(100 * (start_mem - end_mem)
                                        / start_mem))

    return df