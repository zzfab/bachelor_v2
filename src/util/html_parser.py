import base64
from settings import setting
import os


def img_tag(filename):
    return  '<img src="{0}">'.format(filename)

def parse_img_to_html():
    with open(os.path.join(setting['html'],'report.html'),'a') as html_file:
        for file in os.listdir(setting['report']):
            if file.endswith(".png"):
                html_file.write(img_tag(os.path.join(setting['report'],file)))