import sys
import os
import re

root = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
sys.path.append(root)

from settings import setting

#ToDo: Refactoring und in shellskript packen, damit ausfuehrung nach spezifischen schritt in r skript ausgefuehrt wird /pipelining
expression = r'entry: (.*) display type is either not'
expression_2 = r'supported or entry is not found.'
expression_3 = r'supportedorentryisnotfound.'
expression_4 = r'entry:(.*)displaytypeiseithernot'
for file in os.listdir(setting['paths']['data']):
    if file.endswith(".fa"):
        print("Read File")
        print(file)
        with open(os.path.join(setting['paths']['data'],file),'rb+') as f:
            text = f.read().decode("latin-1")
            f.seek(0)
            temp = re.sub(expression," ",text)
            temp = re.sub(expression_2," ",temp).replace(' ','')
            temp = re.sub(expression_3," ",temp).replace(' ','')
            temp = re.sub(expression_4," ",temp).replace(' ','')
            f.write(temp.encode())
            f.truncate()
    else:
        print("Not Read following file")
        print(file)
