from sklearn.metrics import average_precision_score,classification_report,confusion_matrix
import os
import joblib
from sklearn.metrics import roc_auc_score,auc,roc_curve
import json
from datetime import date
import matplotlib.pyplot as plt


#from src.visualize.spells import cast_auc_plot
import pandas as pd
import numpy as np


result_table = pd.DataFrame(columns=['classifiers', 'dataset', 'fpr','tpr','se','sp','acc','mcc','auc'])


class NumpyArrayEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

def predictor(dictionary, model, key):
    if 'Sequential' in model.__class__.__name__:
        X_test = dictionary['X_test']

        y_pred = model.predict_classes(X_test)
    else:
        # Actual class predictions
        y_pred = model.predict(dictionary['X_test'])

    try:
        tn, fp, fn, tp = confusion_matrix(dictionary['y_test'], y_pred).ravel()
        specificity = tn / (tn + fp)
        sensifity = tp / (tp + fn)
        acc = (tp + tn) / (tp + tn +fn+fp)
        mcc = (tp * tn - fp * fn) / np.sqrt((tp+fp) * (tp+fn) * (tp + fp) * (tp + fn))
    except:
        specificity = 0
        sensifity = 0
        acc = 0
        mcc = 0

    fpr, tpr, _ = roc_curve(dictionary['y_test'], y_pred)
    auc = roc_auc_score(dictionary['y_test'], y_pred)
    # Define a result table as a DataFrame
    global result_table
    result_table = result_table.append({'classifiers': str(model.__class__.__name__),
                                        'dataset': key,
                                        'fpr': fpr,
                                        'tpr': tpr,
                                        'se': specificity,
                                        'sp' : sensifity,
                                        'acc': acc,
                                        'mcc': mcc,
                                        'auc': auc}, ignore_index=True)

    return result_table


def eval(dictionary,setting):
    pred_auc_scores = {}
    for key in dictionary.keys():
        for i in os.listdir(setting['paths']['models']):
            if key in i:
                print('*' * 20 + "Start prediction for {} and model {}".format(key,i) + '*' * 20)
                loaded_model = joblib.load(os.path.join(setting['paths']['models'], i))
                result_table = predictor(dictionary[key], loaded_model, key)
                # Set name of the classifiers as index labels

    #cast_auc_plot(pred_auc_scores)
    #open score json
    with open(os.path.join(setting['paths']['scores'], 'scores_{}.json'.format(date.today().strftime("%b-%d-%Y"))),'w') as json_file:
        json.dump(result_table.to_dict(), json_file, indent=4, cls=NumpyArrayEncoder)
    print("[+].src.util.predictor: eval done")