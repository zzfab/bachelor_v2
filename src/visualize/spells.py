from settings import setting
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score, auc,  average_precision_score
from sklearn.metrics import roc_curve
from src.config.config import config
from sklearn.decomposition import SparsePCA
from sklearn.feature_selection import RFE
import seaborn as sns
import os
import numpy as np
from sklearn.manifold import TSNE
import json
import pandas as pd

from src.math.statistics import bonferroni_correction
current_palette = sns.color_palette()
sns.set_style("whitegrid")

def plot_corr_all(corr_results):
    fig, axs = plt.subplots(len(corr_results.keys()), 1, sharey=True, figsize=(16, 9))
    counter = 0
    for i in setting['resolutions']:
        if counter <= len(corr_results.keys()):
            corr_results[i]['corr_dna_{}'.format(i)].plot(kind='line'
                                                          , ax=axs[counter]
                                                          , color='b'
                                                          , title='corr_dna_{}'.format(i))
            counter += 1


def pca_log_reg(df, config):
    feat_cols = df.columns[0:len(df.columns) - 2]
    pca = SparsePCA(n_components=config['dim_reduce']['n_components'],random_state=42)
    pca_result = pca.fit_transform(df[feat_cols].values)
    print(f"Shape of {pca_result.shape}")
    df['pca-one'] = pca_result[:, 0]
    df['pca-two'] = pca_result[:, 1]

    print('Explained variation per principal component: {}'.format(pca.explained_variance_ratio_))

    return df, pca


def plot_pca(df,name):
    plt.figure(figsize=(16, 9))
    swarm = sns.scatterplot(
        x="pca-one", y="pca-two",
        hue="target",
        palette=sns.color_palette("hls", 2),
        data=df,
        legend="full",
        alpha=0.3
    )
    fig = swarm.get_figure()
    fig.savefig(os.path.join(setting['paths']['reports'], 'pca_{}.png'.format(name)))
    plt.close()


def plot_tsne(df, name,axes ):
    # Set up the matplotlib figure
    plt.figure(figsize=(16, 9))
    swarm = sns.scatterplot(
        x="tsne-2d-one", y="tsne-2d-two",
        hue="target",
        palette=sns.color_palette("hls", 2),
        data=df,
        legend="full",
        alpha=0.3,
        ax=axes
    )
    fig = swarm.get_figure()
    fig.savefig(os.path.join(setting['paths']['reports'], 'tsne_{}.png'.format(name)))
    plt.close()

def cast_pca(df, name, config):
    df, pca = pca_log_reg(df, config)
    #
    plot_pca(df,name)


def cast_t_sne(df, name,seed=config['dim_reduce']['seed'], n_components=config['dim_reduce']['n_components'],
                   perplexity=config['dim_reduce']['perplexity'] ):
    # set seed
    np.random.seed(seed)
    counter = 0

    f, axes = plt.subplots(len(perplexity), 1, sharex=True, sharey=True, figsize=(16, 9))
    for p in perplexity:
        tsne = TSNE(n_components=n_components,  perplexity=p, n_iter=300)
        tsne_results = tsne.fit_transform(df)
        df['tsne-2d-one'] = tsne_results[:, 0]
        df['tsne-2d-two'] = tsne_results[:, 1]
        plot_tsne(df, name,axes[counter] )
        counter += 1

def plot_bonferroni_corr_logn(dicitionary):
    bfc_dic = bonferroni_correction(dicitionary, alpha=0.05)
    for key in dicitionary.keys():
        arr = bfc_dic[key]
        arr = np.array([np.log(xi) for xi in arr])

        arr_2d = np.reshape(arr,(int((np.sqrt(len(arr)))),int((np.sqrt(len(arr))))))

        plt.figure(figsize=(16, 9))
        ax = sns.heatmap(arr_2d,xticklabels=False,yticklabels=False)
        #ax.set_title(key)
        fig = ax.get_figure()
        print("[+].bonferroni: save fig {}".format(key))
        fig.savefig(os.path.join(setting['paths']['reports'], 'logn_{}.png'.format(key)))
        plt.close()


def plot_auc(setting):
    #open score json
    with open(os.path.join(setting['paths']['scores'], 'scores.json')) as json_file:
        data = json.load(json_file)


    #set plotting parameters
    resolution = setting['resolutions']
    rows = [x for x in range(len(resolution))]
    res_map_dic = dict(zip(resolution, rows))
    models = setting['models']
    model_dic = dict(zip(models, [x for x in range(len(models))]))

    #define auc plot function
    def plot_auc_func(axs, c_row, c_col, data, i, j, c, model, ms=9):
        if "dna" in i:
            if "_0" not in i and str(j) in i:
                axs[c_row, c_col].plot(i,
                                       data[i],
                                       marker='o',
                                       label='DNA',
                                       color=c,
                                       markersize=ms)
                axs[c_row, c_col].set_title("{model} res = {j}".format(model=model, j=j),
                                            fontdict={'fontsize': 8, 'fontweight': 'medium'})
            else:
                pass
        else:
            if "_0" not in i and str(j) in i:
                axs[c_row, c_col].plot(i,
                                       data[i],
                                       marker='s',
                                       label='Protein sf: 1',
                                       color=c,
                                       markersize=ms)
            elif "_0" in i and str(j) in i:
                axs[c_row, c_col].plot(i,
                                       data[i],
                                       marker="v",
                                       label='Protein sf: 0.5',
                                       color=c,
                                       markersize=ms)

    fig, axs = plt.subplots(nrows=len(rows), ncols=2, figsize=(16, 9), sharey=True)
    plt.subplots_adjust(wspace=.1, hspace=.1)
    for j in resolution:
        c_row = res_map_dic[j]
        for i in data.keys():
            if i.startswith('rf'):
                c_col = model_dic['rf']
                plot_auc_func(axs, c_row, c_col, data, i, j, 'b',"Random Forest")
            elif i.startswith('lr'):
                c_col = model_dic['lr']
                plot_auc_func(axs, c_row, c_col, data, i, j, 'g' ,"Logistic Regression")
            else:
                pass

    for i in range(2):
        for j in range(2):
            axs[i, j].grid()
            axs[i, j].axes.get_xaxis().set_visible(False)

    labels = []

    axLine, axLabel = axs[0, 0].get_legend_handles_labels()
    labels.extend(axLabel)

    fig.legend(labels,
               loc='upper right')

    plt.savefig(os.path.join(setting['paths']['reports'], 'auc.png'))


