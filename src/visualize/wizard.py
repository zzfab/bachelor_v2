from src.visualize.spells import plot_auc,cast_t_sne,cast_pca,plot_bonferroni_corr_logn
from settings import setting
from src.config import config
import numpy as np


def cast_wizard(df_dic):
    #cast cluster viz

    #plot bonferroni corrected logn(xi)
    plot_bonferroni_corr_logn(df_dic)

    for k,v in df_dic.items():
        #print("cast {} {}".format(k,v))
        cast_t_sne(df_dic[k], k)
        #cast_pca(df_dic[k], k, config)

    plot_auc(setting)
    print("[+].src.vizualize.wizard: cast wizard done")
