import os
import sys
import importlib
import numpy as np
import json
import logging
import os



root = os.path.dirname(__file__)
sys.path.append(root)
logging.basicConfig(filename=os.path.join(root,'logfile.log'),
                              level=logging.INFO ,
                              format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


setting = {
    'paths': {
        'wd' : root,
        'data' : os.path.join(root,'data/intern'),
        'prep_data' : os.path.join(root,'data/prep_data'),
        'logs' : os.path.join(root,'logs'),
		'reports' : os.path.join(root,'reports'),
        'models' : os.path.join(root,'models'),
        'scores':os.path.join(root,'data/scores'),
        'html':os.path.join(root,'src/assets/html')
        },
    'resolutions' : np.arange(50,200,50),
    'mode' : { #ToDo build arg for mode
        'prep': False, #
        'viz': False,
        'pred' : True,
        'train' : False,
    },
    'models' : {
        'rf' : False,
        'svm': False,
        'cnn': False,
        'lr': False,
        'ridge_cv':False,
        'ridge':True,
    },
    'colors' : ['Greens','Reds','Blues','Yellows']
}
print(f"read in setting {setting}")
