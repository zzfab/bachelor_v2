import sys
import os
import logging
import pandas as pd

from settings import setting

import os
#Import block from src
from src.util.helper import read_data,convert_to_pkl
from src.util.preparator import union,prepare_training
from src.util.helper import read_data,convert_to_pkl,iter_red_mem
from src.visualize.wizard import cast_wizard
from src.config.config import config
from src.util.trainer import train_logistic_regressin,train_random_forest
from src.util.predictor import eval
from src.util.trainer import run_training
from src.math.statistics import bonferroni_correction
from src.visualize.spells import plot_bonferroni_corr_logn
#from src.util.pdf_generator import create_pdf


import time

def run_engine(setting):
    t0 = time.time()


    if setting['mode']['prep']:
        convert_to_pkl(setting)
        prepared_data = union(read_data(setting['paths']['data']))
        #reduce size of dataframes
        prepared_data = iter_red_mem(prepared_data)
        split_dic = prepare_training(prepared_data)
    else:
        prepared_data = read_data(setting['paths']['prep_data'])
        split_dic = prepare_training(prepared_data)

    if setting['mode']['viz']:
        cast_wizard(prepared_data)
        print(f'[+].run_engine: viz mode done in {time.time()-t0}')
    elif setting['mode']['train']:
        #del prepared_data
        run_training(split_dic)
        print(f'[+].run_engine: train mode done in {time.time()-t0}')
    elif setting['mode']['pred']:
        #del prepared_data
        eval(split_dic, setting)
        print(f'[+].run_engine: pred mode done {time.time()-t0}')
    else:
        print(f'[+].run_engine: prep mode done {time.time()-t0}')

    #write out to pdf
    #create_pdf()

    t1 = time.time()

    print("[+].run.engine: engine running took {}".format(round(t1-t0,2)))
run_engine(setting)